package contactServer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import utils.Domains;
import utils.Logger;

public class ContactSearchRequestHandler implements Runnable {

	private String clientAddress;
	private String contactServerAddress;
	private long clientHandlerId;

	public ContactSearchRequestHandler(byte[] clientAddressData,
			String contactServerAddress) {
		this.clientAddress = new String(clientAddressData).split("@")[0];
		this.contactServerAddress = contactServerAddress;
		this.clientHandlerId = this.hashCode();
	}

	@Override
	public void run() {
		String clientHandlerId = "[id=" + this.clientHandlerId + "]";
		
		try {
			Logger.log("Handling new client request " + clientHandlerId);
			//The contact server address in byte array.
			byte[] myAddress = this.contactServerAddress.getBytes();
			
			
			Logger.log("Creating a response packet to " + clientHandlerId);
			//Creates a datagram response packet.
			DatagramPacket responsePacket = new DatagramPacket(myAddress,
					myAddress.length);
			
			Logger.log("Setting the address for " + clientHandlerId);
			//Sets the response datagram packet destination to be the client.
			responsePacket.setAddress(InetAddress.getByName(clientAddress));
			
			Logger.log("Setting the port for " + clientHandlerId);
			//Sets the response datagram packet destination port.
			responsePacket.setPort(Domains.CLIENTCONTACTPORT);
			
			Logger.log("Creating a datagram socket to send the response to " + clientHandlerId);
			//Creates a new datagram socket to send the packet to the client
			DatagramSocket response = new DatagramSocket();
			
			Logger.log("Sending the packet " + clientHandlerId);
			//Sends the packet to the client.
			response.send(responsePacket);
			
			Logger.log("Closing resources for handler " + clientHandlerId);
			//Closes the socket.
			response.close();
			
			Logger.log("Completed request handling " + clientHandlerId);
			
			
		} catch (IOException e) {
			Logger.log("Something has gone wrong " + clientHandlerId);
		}
	}
}
